# Pd Simple MIDI Message Format

Albert Gräf <aggraef@gmail.com>

Some of my Pd externals, such as [pd-faust][], [pd-faustgen2][] and
[pd-lv2plugin][], process MIDI messages in a symbolic format which I call the
Pd *Simple MIDI Message Format* or *SMMF* for short. But the SMMF format
itself isn't tied in any way to these externals, so I'm documenting it here
and also provide some useful helper abstractions, so that anyone who needs to
process MIDI data in Pd can use it for their own projects.

[Pure]: https://agraef.github.io/pure-lang/
[pd-faust]: https://agraef.github.io/pure-docs/pd-faust.html
[pd-faustgen2]: https://github.com/agraef/pd-faustgen
[pd-lv2plugin]: https://bitbucket.org/agraef/pd-lv2plugin

The idea of the SMMF format is quite simply to represent MIDI data from Pd's
MIDI interface as Pd meta messages which can be passed around freely as a
single stream of messages, while keeping together all the information of the
original MIDI messages. Moreover, the format uses Pd's somewhat idiosyncratic
order of the data bytes (in particular, the MIDI channel comes last), so that
the translation from and to Pd's MIDI objects is easy. The format is
summarized in the following table (`n` denotes a note or controller number,
`v` the velocity or controller value, `c` the 1-based MIDI channel number):

Message                  | Meaning
------------------------ | -------------------------
`ctl v n c`              | control change
`note n v c`             | note
`pgm n c`                | program change
`polytouch v n c`        | key pressure
`touch v c`              | channel pressure
`bend v c`               | pitch bend (14 bit value)
`start`, `stop`, `cont`  | system realtime messages
`sysex b1 b2 ...`        | system exclusive message

Note that most of these messages closely correspond to the inlets and outlets
of Pd's MIDI objects, so converting between these and the SMMF format is
straightforward. In addition, SMMF includes representations for system
exclusive and some useful system real-time messages which don't have a direct
counterpart in Pd. In this folder you can find two little helper patches
(midi-input.pd and midi-output.pd) which handle the conversion, you might want
to look at these and adjust them for your needs as you see fit.

## Examples

While SMMF isn't tied to any particular scripting language, the format also
makes it very easy to write MIDI externals in either Lua or Pure, using
pattern matching to process the SMMF meta messages. For instance, here's the
Pure definition of the miditransp object which transposes MIDI note and
polyphonic aftertouch messages. It takes a single creation argument, either a
constant (number of semitones to transpose by), or an arbitrary Pure function
to be applied to the note numbers.

    miditransp k::number msg = miditransp (+k) msg;
	miditransp f (note n v c) = note m v c if numberp m when m = f n end;
	miditransp f (polytouch v n c) = polytouch v m c if numberp m when m = f n end;
	miditransp f msg = msg otherwise;

I have put together a little collection of useful examples covering various
common tasks such as MIDI filtering, routing, and playback. You can find these
in the lua and pure folders, along with some demo patches. The Lua examples
require [pd-lua][] to run (this is readily available in [purr-data][]), the
Pure examples [pd-pure][]. Some of the patches also require externals from the
[cyclone][] library (readily available in [pd-extended][], [pd-l2ork][], and
[purr-data][]). The [fluidsynth][] examples need Frank Barknecht's [fluid~][]
external, which is available only in [pd-l2ork][] and [purr-data][] at this
time; we recommend the purr-data version of fluid~ because it has various
improvements and bugfixes, as well as built-in SMMF support.

You'll also need a GM-compatible soundfont to use with fluid~. The examples
assume the [FluidR3][] GM soundfont, which is available in the package
repositories of Arch and Ubuntu Linux and can be downloaded freely from
various locations on the web. Using the updated fluid~ version from purr-data,
you can just copy the file to a directory on the Pd library search path and
you should be set. (If you're stuck with the original fluid~ version for some
reason, you'll have to specify the full path to the soundfont file instead.)

[pd-lua]: https://agraef.github.io/pd-lua/
[pd-pure]: https://agraef.github.io/pure-docs/pd-pure.html
[pd-extended]: http://puredata.info/downloads/pd-extended
[pd-l2ork]: http://l2ork.music.vt.edu/main
[purr-data]: https://agraef.github.io/purr-data/
[cyclone]: http://puredata.info/downloads/cyclone/
[fluidsynth]: http://www.fluidsynth.org/
[fluid~]: http://puredata.info/Members/fbar/index_html
[FluidR3]: http://http.debian.net/debian/pool/main/f/fluid-soundfont/

## Caveats

There are some quirks with Pd's MIDI subsystem (cf. issues [#1272][1272] and
[#1262][1262] on the old Pd project page) which you should be aware of. These
have nothing to do with SMMF per se, but they affect the midi-input and
midi-output helper abstractions distributed with this package and, as they
don't really seem to be documented anywhere (apart from these bug reports and
some really old mailing list posts), I'm doing that here.

[1272]: https://sourceforge.net/p/pure-data/bugs/1272/
[1262]: https://sourceforge.net/p/pure-data/bugs/1262/

- In Linux Pd versions up to (and including) Pd 0.47, output of raw MIDI data
  like sysex messages via ALSA is broken. This bug was finally fixed in Pd
  0.48, so if you're still running an older Pd version, we highly recommend
  updating your installation to the latest Pd version.

- Pd's bendin object produces an unsigned value range of 0 thru 16383, while
  the bendout object expects a signed range of -8192 thru +8191. Which means
  that you have to translate the values when routing pitch bends from MIDI
  input to MIDI output. This bug appears to have been there forever and it
  affects *all* platforms. Because it has been around for so long, it won't
  ever be fixed in vanilla, in order to maintain backwards compatibility.

Both issues have long been fixed in [pd-l2ork][] and [purr-data][], which we
recommend. If you prefer to use vanilla, you just need to be aware that the
output of bendin may differ depending on which Pd variant you use. However,
recent releases of [pd-l2ork][] and [purr-data][] provide an extra option for
the bendin object which makes it operate in a vanilla-compatible way. This
option is now being used in midi-input.pd so that the abstraction will work
the same no matter which Pd flavor you use, as long as you're running a
current version of these programs.
