
-- This is more or less the same as the stand-alone script at
-- https://bitbucket.org/agraef/sclsyx, but implemented as a Pd external, and
-- ported to Lua.

-- Copyright (c) 2016, 2020 Albert Graef. Copying and distribution of this
-- file, with or without modification, are permitted in any medium without
-- royalty provided the copyright notice and this notice are preserved. This
-- file is offered as-is, without any warranty.

local sclsyx = pd.Class:new():register("sclsyx")

function sclsyx:initialize(sel, atoms)
   self.inlets = 1
   self.outlets = 1

   -- Configure the following as needed/wanted. We should maybe turn these
   -- into user-configurable options of the sclsyx object in the future.
   -- (Usually the defaults below should be fine, though.)

   -- Set this to false for non-realtime tunings; the default, true, means
   -- that tunings will also affect sounding notes.
   self.rt = true

   -- Set this to true for 2-byte tunings (14 bit precision, -100..100 ct
   -- range). Otherwise you get 7 bit precision and a -64..63 range, which
   -- should be good enough for most cases.
   self.b = false

   -- Set this to the desired reference tone. The default value 9 means that A
   -- will be fixed at 0 ct, which is most likely what you want if you wish to
   -- keep to standard concert pitch.
   self.r = 9

   return true
end

-- Read a Scala file and return the resulting tuning, along with the
-- description. Returns an error message instead in case of a parse error, or
-- if the scale isn't octave-based.

function sclsyx:read(filename)
   local i,j,n = 0,1,0
   local descr = ""
   local t = {}
   for line in io.lines(filename) do
      if string.sub(line, 1, 1) ~= "!" then
	 if i == 0 then
	    descr = line
	    i = 1
	 elseif i == 1 then
	    n = tonumber(line)
	    if not n then
	       return "Scala file format error"
	    elseif n ~= 12 then
	       return "Not a 12-tone scale"
	    end
	    i = 2
	 else
	    local p,q = string.match(line, "^%s*(%d+)/(%d+)%s*$")
	    if p and q then
	       p, q = tonumber(p), tonumber(q)
	       t[j] = 1200 * math.log(p/q, 2)
	    else
	       p = string.match(line, "^%s*(%d+)%s*$")
	       if p then
		  p = tonumber(p)
		  t[j] = 1200 * math.log(p, 2)
	       else
		  t[j] = tonumber(line)
	       end
	    end
	    if not t[j] then
	       return "Scala file format error"
	    else
	       t[j] = t[j] - j*100
	    end
	    if j == 12 then
	       -- ignore any extra garbage at end
	       break
	    end
	    j = j+1
	 end
      end
   end
   if #t < n then
      return "Scala file format error"
   elseif t[n] ~= 0 then
      return "Not a 12-tone scale"
   else
      -- remove the octave that we don't need, and add the implicit unison at
      -- the beginning
      table.remove(t, 12)
      table.insert(t, 1, 0.0)
      return descr, t
   end
end

-- Convert a Scala file to an MTS 1 or 2 byte scale/octave tuning.

function sclsyx:convert(filename)
   local rt, b, r = self.rt, self.b, self.r
   local descr, t = self:read(filename)
   if t then
      -- shift the tuning so that the reference tone is at 0 cent
      r = (r<0 and 0) or (r>11 and 11) or r
      local t0 = t[r+1]
      for i = 1, #t do
	 t[i] = t[i] - t0
      end
      -- encode the tuning
      local u = {}
      u[1] = 0x7e + (rt and 1 or 0) -- realtime / non-realtime
      u[2] = 0x7f -- device id (any device)
      u[3] = 8 -- MTS tuning
      u[4] = 8 + (b and 1 or 0) -- 1/2 byte octave-based
      u[5] = 3; u[6] = 0x7f; u[7] = 0x7f -- MIDI channel mask (all channels)
      if b then
	 -- In the 2-byte encoding the value denotes a single 14 bit number in
	 -- the range 0..16383 (most significant byte first). 0 denotes -100
	 -- cent, 8192 is the center (0 cent) and 16384 would be +100 cent
	 -- (corresponding to an effective resolution of 100/2^14 == .012207
	 -- cent).
	 local function val(x)
	    local y = math.floor(x*8192.0/100.0+0.5) + 8192
	    y = (y < 0 and 0) or (y > 16383 and 16383) or y
	    local msb = y >> 7
	    local lsb = y - (msb << 7)
	    return msb, lsb
	 end
	 for i = 1, #t do
	    local msb, lsb = val(t[i])
	    u[2*i+6] = msb
	    u[2*i+7] = lsb
	 end
      else
	 -- In the 1-byte encoding we simply have cent values in the range
	 -- 0..127 where 64 is the center (0 cents).
	 local function val(x)
	    local y = math.floor(x+0.5) + 64
	    y = (y < 0 and 0) or (y > 127 and 127) or y
	    return y
	 end
	 for i = 1, #t do
	    u[i+7] = val(t[i])
	 end
      end
      return descr, u
   else
      return descr
   end
end

-- Pd external. This takes a Scala (.scl) filename on the inlet and outputs
-- the corresponding sysex message in SMMF format. Various configuration
-- options can be set in the member variables at the beginning of the script.

function sclsyx:in_1_symbol(filename)
   local descr, t = self:convert(filename)
   if t then
      pd.post(descr)
      self:outlet(1, "sysex", t)
   else
      self:error(descr)
   end
end
