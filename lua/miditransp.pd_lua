
-- Little helper external to transpose MIDI messages in the SMMF format.
-- Takes a single creation argument, either the number of semitones (positive
-- or negative) to transpose by, or an arbitrary Lua function to be mapped on
-- the note numbers.

local miditransp = pd.Class:new():register("miditransp")

function miditransp:initialize(sel, atoms)
   self.inlets = 1
   self.outlets = 1
   if type(atoms[1]) == "number" then
      self.offs = atoms[1]
   else
      -- Lua function to apply to the note number
      local g = load("return " .. table.concat(atoms, " "))
      if type(g) == "function" and pcall(g) then
	 self.fun = g()
      else
	 self:error("miditransp: not a Lua function: " ..
		    table.concat(atoms, " "))
      end
   end
   return true
end

function miditransp:transp(n)
   if self.offs then
      return n+self.offs
   elseif self.fun then
      return self.fun(n)
   else
      return n
   end
end

-- transpose notes and polytouch messages
function miditransp:in_1_note(atoms)
   local n,v,c = table.unpack(atoms)
   self:outlet(1, "note", {self:transp(n),v,c})
end

function miditransp:in_1_polytouch(atoms)
   local v,n,c = table.unpack(atoms)
   self:outlet(1, "polytouch", {v,self:transp(n),c})
end

-- anything else goes through unchanged
function miditransp:in_1(sel, atoms)
   self:outlet(1, sel, atoms)
end
