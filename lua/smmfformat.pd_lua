
-- This object takes input in the same format as the cyclone midiformat
-- object, but converts it to SMMF rather than raw MIDI data. It can be used
-- in conjunction with the cyclone midiparse object to translate a raw stream
-- of MIDI bytes (which is produced, e.g., by midiin or the seq object) to
-- SMMF. Note that the inlets are in exactly the same order as the outlets of
-- midiparse, so you just wire them straight through.

local smmfformat = pd.Class:new():register("smmfformat")

function smmfformat:initialize(sel, atoms)
   self.inlets = 7
   self.outlets = 1
   self.ch = 1
   -- Optional arg specifies the default MIDI channel (last inlet).
   if type(atoms[1]) == "number" then
      self.ch = atoms[1]
   end
   return true
end

function smmfformat:in_1_list(atoms)
   local n,v = table.unpack(atoms)
   self:outlet(1, "note", {n,v,self.ch})
end

function smmfformat:in_2_list(atoms)
   local v,n = table.unpack(atoms)
   self:outlet(1, "polytouch", {v,n,self.ch})
end

function smmfformat:in_3_list(atoms)
   local v,n = table.unpack(atoms)
   self:outlet(1, "ctl", {v,n,self.ch})
end

function smmfformat:in_4_float(n)
   -- cyclone expects raw program numbers here, so we do the same
   self:outlet(1, "pgm", {n+1,self.ch})
end

function smmfformat:in_5_float(v)
   self:outlet(1, "touch", {v,self.ch})
end

function smmfformat:in_6_float(v)
   self:outlet(1, "bend", {v,self.ch})
end

function smmfformat:in_7_float(c)
   self.ch = c
end
