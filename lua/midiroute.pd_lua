
-- Route MIDI messages according to their MIDI channel. Each channel listed in
-- the creation arguments gets its own outlet, the rightmost outlet receives
-- any unmatched messages.

local midiroute = pd.Class:new():register("midiroute")

function midiroute:initialize(sel, atoms)
   self.inlets = 1
   self.outlets = #atoms+1
   self.channels = atoms
   for i = 1, #atoms do
      if type(atoms[i]) ~= "number" then
	 self.error("midiroute: not a number: " .. tostring(atoms[i]))
	 return false
      end
   end
   return true
end

function midiroute:in_1(sel, atoms)
   if #atoms > 1 and #atoms < 4 and sel ~= "sysex" then
      local c = atoms[#atoms]
      if type(c) == "number" then
	 for i = 1, #self.channels do
	    if self.channels[i] == c then
	       self:outlet(i, sel, atoms)
	       return
	    end
	 end
      end
   end
   self:outlet(#self.channels+1, sel, atoms)
end
