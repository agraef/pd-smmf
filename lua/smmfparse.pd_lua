
-- This is the inverse of smmfformat; it takes input in SMMF format and
-- outputs it on various outlets in the same way as midiparse from the cyclone
-- library. You can use this, e.g., in conjunction with midiformat to produce
-- a stream of raw MIDI bytes from SMMF. Note that the outlets are in exactly
-- the same order as the inlets of midiformat, so you just wire them straight
-- through.

local smmfparse = pd.Class:new():register("smmfparse")

function smmfparse:initialize(sel, atoms)
   self.inlets = 1
   self.outlets = 7
   return true
end

function smmfparse:in_1_note(atoms)
   local n,v,c = table.unpack(atoms)
   self:outlet(7, "float", {c})
   self:outlet(1, "list", {n,v})
end

function smmfparse:in_1_note(atoms)
   local n,v,c = table.unpack(atoms)
   self:outlet(7, "float", {c})
   self:outlet(1, "list", {n,v})
end

function smmfparse:in_1_polytouch(atoms)
   local v,n,c = table.unpack(atoms)
   self:outlet(7, "float", {c})
   self:outlet(2, "list", {v,n})
end

function smmfparse:in_1_ctl(atoms)
   local v,n,c = table.unpack(atoms)
   self:outlet(7, "float", {c})
   self:outlet(3, "list", {v,n})
end

function smmfparse:in_1_pgm(atoms)
   local n,c = table.unpack(atoms)
   self:outlet(7, "float", {c})
   -- cyclone reports raw program numbers here, so we do the same
   self:outlet(4, "float", {n-1})
end

function smmfparse:in_1_touch(atoms)
   local v,c = table.unpack(atoms)
   self:outlet(7, "float", {c})
   self:outlet(5, "float", {v})
end

function smmfparse:in_1_bend(atoms)
   local v,c = table.unpack(atoms)
   self:outlet(7, "float", {c})
   self:outlet(6, "float", {v})
end

-- ignore everything else
function smmfparse:in_1(sel, atoms)
end
