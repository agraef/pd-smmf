
-- Set the MIDI channel of a message. Takes a single creation argument, either
-- a constant MIDI channel number, or a numeric function to be mapped on the
-- channel numbers.

local midichan = pd.Class:new():register("midichan")

function midichan:initialize(sel, atoms)
   self.inlets = 1
   self.outlets = 1
   if type(atoms[1]) == "number" then
      self.ch = atoms[1]
   else
      -- Lua function to apply to the channel number
      local g = load("return " .. table.concat(atoms, " "))
      if type(g) == "function" and pcall(g) then
	 self.fun = g()
      else
	 self:error("midichan: not a Lua function: " ..
		    table.concat(atoms, " "))
      end
   end
   return true
end

function midichan:chan(n)
   if self.ch then
      return self.ch
   elseif self.fun then
      return self.fun(n)
   else
      return n
   end
end

function midichan:in_1(sel, atoms)
   if #atoms > 1 and #atoms < 4 and sel ~= "sysex" then
      atoms[#atoms] = self:chan(atoms[#atoms])
   end
   self:outlet(1, sel, atoms)
end
