
-- Little helper external to filter MIDI messages in the SMMF format.
-- Takes a single creation argument, either a MIDI channel number, or a number
-- predicate which determines the messages to be passed through.

local midifilter = pd.Class:new():register("midifilter")

function midifilter:initialize(sel, atoms)
   self.inlets = 1
   self.outlets = 1
   if type(atoms[1]) == "number" then
      self.ch = atoms[1]
   else
      -- Lua function to apply to the channel number
      local g = load("return " .. table.concat(atoms, " "))
      if type(g) == "function" and pcall(g) then
	 self.fun = g()
      else
	 self:error("midifilter: not a Lua function: " ..
		    table.concat(atoms, " "))
      end
   end
   return true
end

function midifilter:filter(n)
   if self.ch then
      return n == self.ch
   elseif self.fun then
      return self.fun(n)
   else
      return true
   end
end

function midifilter:in_1(sel, atoms)
   if #atoms > 1 and #atoms < 4 and sel ~= "sysex" then
      if self:filter(atoms[#atoms]) then
	 self:outlet(1, sel, atoms)
      end
   else
      self:outlet(1, sel, atoms)
   end
end
