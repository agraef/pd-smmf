
-- Output cc 123 on all MIDI channels. This kills hanging notes in
-- GM-compatible synths like fluidsynth.

local panic = pd.Class:new():register("panic")

function panic:initialize(sel, atoms)
   self.inlets = 1
   self.outlets = 1
   return true
end

-- Receive any input (like a bang) to output the sequence.
function panic:in_1(sel, atoms)
   for c = 1, 16 do
      self:outlet(1, "ctl", {1,123,c})
   end
end
