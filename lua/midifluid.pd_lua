
-- Convert SMMF MIDI messages to the format understood by fluid~, which uses
-- other selectors and has the arguments in a different order.

local midifluid = pd.Class:new():register("midifluid")

function midifluid:initialize(sel, atoms)
   self.inlets = 1
   self.outlets = 1
   return true
end

function midifluid:in_1_note(atoms)
   local n,v,c = table.unpack(atoms)
   self:outlet(1, "note", {c,n,v})
end

function midifluid:in_1_ctl(atoms)
   local v,n,c = table.unpack(atoms)
   self:outlet(1, "control", {c,n,v})
end

function midifluid:in_1_pgm(atoms)
   local n,c = table.unpack(atoms)
   self:outlet(1, "prog", {c,n})
end

function midifluid:in_1_bend(atoms)
   local v,c = table.unpack(atoms)
   self:outlet(1, "bend", {c,v})
end

-- No (poly)touch, no sysex, fluid~ doesn't support these.
-- We ignore everything else to be on the safe side.
function midifluid:in_1(sel, atoms)
end
